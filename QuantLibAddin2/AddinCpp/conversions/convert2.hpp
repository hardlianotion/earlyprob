//
//#ifndef addincpp_convert2_hpp
//#define addincpp_convert2_hpp
//
//#include <oh/property.hpp>
//#include <oh/conversions/convert2.hpp>
//#include <ql/time/date.hpp>
//
//// QuantLibAddin-specific specializations of templates in the ObjectHandler convert2.hpp file.
//
//namespace ObjectHandler {
//
//    template<> 
//    QuantLib::Date convert2<QuantLib::Date, property_t>(const property_t& c);
//}
//
//#endif
//
